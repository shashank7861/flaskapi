import os
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_root():
   x= os.getcwd()
#    print x
   return x

@app.route('/flask')
def hello_flask():
   return 'Hello Flask'

@app.route('/python/')
def hello_python():
   return 'Hello Python'

if __name__ == '__main__':
   app.run()